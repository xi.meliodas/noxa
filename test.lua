---------------------------------------- Includes ------------------------------
local Dbg = require("../Modules.Logger")
---------------------------------------- GLOBAL VARIABLES ----------------------
local mon = peripheral.find("monitor")
Dbg.setOutputTerminal(mon)
---------------------------------------- FUNCTIONS -----------------------------

---------------------------------------- MAIN ----------------------------------
-- load git
if not fs.exists("Modules/Git.lua") then
    --34501246
    print("DownLoading Git")
    local response = http.get("https://gitlab.com/api/v4/projects/34503720/repository/files/Modules%2FGit.lua/raw?ref=master")
    if response then
        local contents = response.readAll()
        response.close()
        local file = fs.open("Modules/Git.lua", "w")
        file.write(contents)
        file.close()
    else
        error("could not download Git")
    end
end
local Git = require("Modules.Git")
--git load finished
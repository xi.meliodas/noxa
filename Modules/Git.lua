---------------------------------------- Includes ------------------------------

---------------------------------------- GLOBAL VARIABLES ----------------------

---------------------------------------- FUNCTIONS -----------------------------

---------------------------------------- MAIN ----------------------------------
--[[ include the snippet below at the top of every "main" to ensure the apiloader is installed, initialized and updated
-- load git
if not fs.exists("Modules/Git.lua") then
    --34501246
    print("DownLoading Git")
    local response = http.get("https://gitlab.com/api/v4/projects/34503720/repository/files/Modules%2FGit.lua/raw?ref=master")
    if response then
        local contents = response.readAll()
        response.close()
        local file = fs.open("Modules/Git.lua", "w")
        file.write(contents)
        file.close()
    else
        error("could not download Git")
    end
end
local Git = require("Modules.Git")
--git load finished
--]]


local gitUrl = "https://gitlab.com"
local gitFileUrl = "api/v4/projects/projectID/repository/files"
local branch = "master"

local Git = {}

Git.computerCraftProjectID = 34503720
---Download file from Gitlab and return Contents
---@param projectID number projectId
---@param file string file path and location on git
---@return string | nil contents response or nil on failure
function Git.downloadFileFromGit(projectID, file)
    local url = gitUrl .. "/" .. gitFileUrl
    url = string.gsub(url, "projectID",tostring(projectID))
    url = url .. "/" .. textutils.urlEncode(file) .. "/raw?ref=" .. branch
    local response = http.get(url)
    if response then
        local contents = response.readAll()
        response.close()
        return contents
    else
        return nil
    end
end

---Download From Gitlab
---@param projectID number gitlab projectID to download from
---@param file string file with filepath to download
function Git.getFile(projectID, file)
    print("DownLoading " .. file)
    local response = Git.downloadFileFromGit(projectID, file)
    if response == nil then
        error("Something went wrong While Downloading " .. file)
    end
    local fh = fs.open(file, "w")
    fh.write(response)
    fh.close()
end

---Check if File Exists and if not Download From Gitlab
---@param projectID number gitlab projectID to download from
---@param file string file with filepath to download
function Git.getFileifNeeded(projectID,file)
    if type(file) ~= "string" or type(projectID) ~= "number" then
        error("Failed To get File From GitLab, Invalid Argument " .. file, 1)
    end
    if fs.exists(file) then
        return
    end
    Git.getFile(projectID,file)
end

return Git














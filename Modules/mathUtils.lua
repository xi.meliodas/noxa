local expect = require("cc.expect")


--- Adds a and b togther
---@param a number first number.
---@param b number number to add.
---@return number sum (a + b)
function Add(a,b)
    expect.expect(1,a,"number")
    expect.expect(2,b,"number")
    return a + b
end

--- Subtracts b from a
---@param a number number.
---@param b number number to Subtract.
---@return number result (a - b)
function Sub(a,b)
    expect.expect(1,a,"number")
    expect.expect(2,b,"number")
    return a - b
end

--- Divides b into a
---@param a number number.
---@param b number number to Divide By.
---@return number result (a / b)
function Div(a,b)
    expect.expect(1,a,"number")
    expect.expect(2,b,"number")
    return a / b
end

--- Multiplies a and b
---@param a number number.
---@param b number number to Multiply by.
---@return number result (a * b)
function Mult(a,b)
    expect.expect(1,a,"number")
    expect.expect(2,b,"number")
    return a * b
end

--- remainder of division of a and b
---@param a number number.
---@param b number number to divide by.
---@return number result (a % b)
function Modu(a,b)
    expect.expect(1,a,"number")
    expect.expect(2,b,"number")
    return a % b
end

